// https://gitlab.com/jsmetana/dmp_template
// SPDX-License-Identifier: CC0-1.0


#let field = (
  s: (
    color: rgb("#555555"),
    code: "23-41-M/01 Strojírenství",
    specialization: "Počítačová podpora konstruování",
  ),
  ea: (
    color: rgb("#3276b1"),
    code: "26-41-M/01 Elektrotechnika",
    specialization: "Mechatronika",
  ),
  eb: (
    color: rgb("#35aa47"),
    code: "26-41-M/01 Elektrotechnika",
    specialization: "Automatizace měst a budov",
  ),
  ita: (
    color: rgb("#d2322d"),
    code: "18-20-M/01 Informační technologie",
    specialization: "Vývoj aplikací",
  ),
  itb: (
    color: rgb("#ffb848"),
    code: "18-20-M/01 Informační technologie",
    specialization: "Správa sítí a IT bezpečnost",
  ),
)

#let code(caption, id: "", number: false, body) = {
  let a = ();
  for (i, line) in body.at("text").split("\n").enumerate() {
    a.push(str(i+1));
  }
  let c = body;
  if number {
    c = stack(
      dir: ltr,
      spacing: .6em,
      {
        set text(gray)
        raw(a.join("\n"))
      },
      style(styles => {
        let len = measure(body, styles);
        line(angle: 90deg, stroke: gray, length: len.height);
      }),
      c,
    );
  }

  pad(y: .5em)[
    #figure(
      caption: caption,
      kind: "code",
      supplement: "Kód",
      box(
        stroke: field.itb.color,
        radius: .5em,
        inset: 1em,
        c
      )
    )
    #label(id)
  ]
}

#let template(conf: none, doc) = {

  // úvodní nastavení dokumentu //
  set document(
    author: conf.author,
    keywords: conf.keywords.en,
    title: conf.title,
  )
  set page(paper: "a4", numbering: "i")
  set text(
    lang: "cs",
    region: "cz",
    font: "New Computer Modern",
    size: 11pt,
    hyphenate: false,
  )
  show heading: set text(font: "Latin Modern Sans")

  // automatické nezlomitelné mezery //
  // show regex("(^k )| k "): [ k~]
  // show regex("(^s )| s "): [ s~]
  // show regex("(^v )| v "): [ v~]
  // show regex("(^z )| z "): [ z~]
  // show regex("(^o )| o "): [ o~]
  // show regex("(^u )| u "): [ u~]
  // show regex("(^a )| a "): [ a~]
  // show regex("(^i )| i "): [ i~]
  // show regex("(^K )| K "): [ K~]
  // show regex("(^S )| S "): [ S~]
  // show regex("(^V )| V "): [ V~]
  // show regex("(^Z )| Z "): [ Z~]
  // show regex("(^O )| O "): [ O~]
  // show regex("(^U )| U "): [ U~]
  // show regex("(^A )| A "): [ A~]
  // show regex("(^I )| I "): [ I~]

  // nahrazení desetinné tečky za čárku v rovnicích
  show math.equation: it => {
    show regex("\d+\.\d+"): it => {
      show ".": {","+h(0pt)}
      it
    }
    it
  }

  // titulní stránka //
  {
    show: page.with(
      margin: (top: 15mm, left: 44mm, right: 26mm, bottom: 19mm),
      background: {
        v(11mm)
        h(37.5mm)
        box(fill: conf.color, height: 271mm, width: 4mm)
        h(1fr)
        v(14mm)
      },
      numbering: none
    )
    set text(
      weight: "semibold",
      font: "Latin Modern Sans",
    )

    {
      show par: set block(spacing: 0.55em)
      text(size: 12pt, conf.work)
      v(3mm)
      image("sps-logo.png")
      v(2mm)
      text(size: 14pt, conf.code)
      v(1fr)
      text(size: 28pt, conf.title)
      v(12mm)
      text(size: 16pt, conf.author)
    }
    v(2fr)
    show par: set block(spacing: 0.65em)
    set text(size: 11pt)

    if conf.supervisor != none [Vedoucí: #conf.supervisor \ ]

    [Zaměření: #conf.specialization \ ]

    conf.date.split(" ").at(1)
  }

  pagebreak()
  pagebreak()

  // zadání //
  {
    set page(numbering: none)
    page(background: {
      box(
        inset: (left: 7mm, right: -7mm, top: 7mm, bottom: -7mm),
        conf.assignment.at(0, default:[Místo pro zadání])
      )
    })[]
    page(background: {
      box(
        inset: (left: -7mm, right: 7mm, top: 7mm, bottom: -7mm),
        conf.assignment.at(1, default:[Místo pro zadání])
      )
    })[]
  }

  // úvodní stránky se dvěma sloupci //
  {
    let margin = (
      outside: 24mm,
      inside: 37mm,
      top: 27.5mm,
      bottom: 22mm
    );
    set page(margin: margin, background: locate(loc => {
      v(23mm)
      if calc.rem(loc.page(), 2) == 0 {
        h(margin.outside)
      } else {
        h(margin.inside)
      }
      box(fill: conf.color, height: 252mm, width: 4mm)
      if calc.rem(loc.page(), 2) == 0 {
        h(margin.inside)
      } else {
        h(margin.outside)
      }
      v(22mm)
    }))

    show heading: set block(below: 1.2em)

    // poděkování a prohlášení //
    columns(2, gutter: 7.5%, {
      align(right, heading(outlined: false)[
        Poděkování
      ])
      par(justify: true, conf.acknowledgements)

      colbreak()

      heading(outlined: false)[
        Prohlášení
      ]
      par(justify: true)[
        Prohlašuji,
        že jsem svou maturitní práci vypracoval samostatně
        a že jsem uvedl veškeré použité informační zdroje
        (literaturu, projekty, SW, atd.),
        které jsou uvedené v seznamu literatury.

        Nemám závažný důvod proti užití tohoto školního díla
        ve smyslu § 60 zákona č. 121/2000 Sb.,
        o právu autorském,
        o právech souvisejících s právem autorským
        a o změně některých zákonů (autorský zákon).

        V Praze, #conf.date
      ]
      v(18mm)
      box(baseline: 100%, {
        line(length: 66mm, stroke: (dash: "loosely-dotted"))
        align(center)[ Podpis autora práce ]
      })
    })
    pagebreak()

    // abstrakt //
    let abstract(side: left, lang: "cs") = {
      set text(lang: lang)
      let (title, keywords) = if lang == "cs" {
        ("Abstrakt", "Klíčová slova")
      } else {
        ("Abstract", "Keywords")
      }
      align(side, heading(outlined: false, title))
      par(justify: true, conf.abstract.at(lang))
      v(6mm)
      text(font: "Latin Modern Sans")[ *#keywords:* ]
      sym.space
      conf.keywords.at(lang)
    }
    columns(2, gutter: 7.5%, {
      abstract(side: right)
      colbreak()
      abstract(lang: "en")
    })
    pagebreak()

    // obsah //
    let contents(side: left, h: "Obsah", ..opt) = {
      style(s => {
        let o = outline(..(title: none, indent: 3mm) + opt.named())
        if 0pt not in measure(o, s).values() {
          align(side, heading(outlined: false, h))
          o
        }
      })
    }
    columns(2, gutter: 7.5%, contents(side: right))
    pagebreak()
    columns(2, gutter: 7.5%, {
      contents(
        side: right,
        h: "Seznam obrázků",
        target: figure.where(kind: image),
      )
      colbreak()

      contents(
        h: "Seznam tabulek",
        target: figure.where(kind: table),
      )
    })
    pagebreak()
    columns(2, gutter: 7.5%, contents(
      side: right,
      h: "Seznam ukázek kódu",
      target: figure.where(kind: "code"),
    ))
  }

  // nadpisy //
  set heading(numbering: "1.1 ")
  show heading: it => stack(
    dir: ltr,
    spacing: 4mm,
    box(
      fill: conf.color,
      height: if it.level == 1 {3em} else {1.3em},
      width: if it.level == 3 {2mm} else {4mm},
    ),
    align(horizon, it),
  )
  show heading: set block(spacing: 8mm)
  show heading.where(level: 1): it => {
    pagebreak(weak: true, to: "odd")
    it
  }

  page(background: none)[]


  // vzhled stránek //
  set text(hyphenate: auto, size: 12pt)
  show par: set block(spacing: 0.64em)
  set par(justify: true, first-line-indent: 1.5em)
  show raw.where(block: false): box.with(
    fill: luma(240),
    inset: (x: 3pt, y: 0pt),
    outset: (y: 3pt),
    radius: 2pt,
  )
  set page(
    numbering: "1",
    footer-descent: 50%,
    margin: (
      outside: 37mm,
      inside: 44mm,
      top: 33mm,
      bottom: 43mm,
    ),
    header: locate(loc => {
      set text(font: "Latin Modern Sans", fill: luma(50%))

      let ahead = query(
        selector(heading.where(level: 1, outlined: true)).after(loc),
        loc,
      )

      if ahead != () and ahead.first().location().page() == loc.page() { return }

      let behind = query(
        selector(heading.where(level: 1, outlined: true)).before(loc),
        loc,
      )
      if behind == () { return }

      let hd() = {
        let hd = behind.last()
        if hd.numbering != none [
          #counter(heading).at(hd.location()).at(0)
          #sym.space.thin
        ]
        hd.body
      }

      let odd = calc.rem(loc.page(), 2) == 1
      set text(dir: rtl) if odd

      if not odd {
        hd()
        h(1em)
      }

      box(
        width: 1fr,
        inset: 2pt,
        line(
          length: if odd {-200%} else {200%},
          stroke: (
            paint: conf.color.lighten(50%),
            thickness: 4pt,
            dash: (4pt, 7pt),
          )
        )
      )

      if odd {
        h(1em)
        hd()
      }
    })
  )
  counter(page).update(1)

  doc

  set par(justify: false)
  bibliography(
    "hayagriva.yml",
    title: "Zdroje",
    style: "iso690-numeric-brackets-cs.csl",
  )
}
