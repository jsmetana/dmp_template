#import "lib.typ": template, field, code
#show: template.with(
  conf: field.itb + (
    work: "Maturitní práce",
    title: "Název práce",
    author: "Jméno Příjmení",
    supervisor: "Ing. Jméno Příjmení",
    date: "březen 2024",
    acknowledgements: [
      Text poděkování ...
    ],
    abstract: (
      cs: [
        ---
      ],
      en: [
        ---
      ],
    ),
    keywords: (
      cs: "čárkou oddělené, slova",
      en: "",
    ),
    assignment: (
      image("zadani-1.png"),
      image("zadani-2.png"),
    ),
  )
)


#heading(numbering: none)[ Úvod ]

#lorem(100)

#figure(caption: [popis obrázku], image("sps-logo.png"))


#heading(numbering: none)[ Závěr ]

#lorem(100)

