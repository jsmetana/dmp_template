# Typst šablona pro praktické maturitní práce na SPŠ Prosek

Neoficiální šablona podle
[školních šablon](https://github.com/sps-prosek/dmp-sablony/)
pro sázecí program [Typst](https://typst.app).

Doporučený způsob použití je nahrát všechny soubory (vyjma README.md)
a vlastní zadání a psát v main.typ.


## Autorské právo

Tato šablona a všechny její části, až na vyjímky,
jsou nabízeny pod licencí
[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.cs).

Logo školy náleží Střední průmyslové škole na Proseku,
a jeho použití je možné pouze za určitých předpokladů.

Zdrojem písem Latin Modern Sans je
[GUST](https://www.gust.org.pl/projects/e-foundry/latin-modern).

Citační styl `iso690-numeric-brackets-cs.csl` je převzat z
[CSL repozitáře stylů](https://github.com/citation-style-language/styles).

S touto šablonou na tyto předměty
nevzniká uživateli jakékoliv právo,
a uživatel za jejich použití
i použití šablony přebírá plnou zodpovědnost,
a to do míry umožněné zákonem.
